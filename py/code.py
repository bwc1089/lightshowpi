import web
import glob
import os
import synchronized_lights_class as lights  #change this to FMsynchronized_lights_class to test fm program
import ConfigParser
#import signal
#import sys
#def signal_handler(signal, frame):
        #slc.cleanup()
        #sys.exit(0)
#signal.signal(signal.SIGINT, signal_handler)

template_dir = os.path.abspath(os.path.dirname(__file__)) + "/templates"
#render = web.template.render('templates/')

slc=lights.slc()

urls= (
    '/', 'index',
	'/ajax','ajax',
	'/getvars','getVars'
)
render = web.template.render(template_dir, globals={'glob':glob,'os':os,'ConfigParser':ConfigParser,'slc':slc})

class index:
    def GET(self):        
		return render.index()

class ajax:
    def GET(self):        
        var = web.input()
        return render.ajax(var)
    def POST(self):        
		var = web.input()
		return render.ajax(var)

class getVars:
    def GET(self):        
		return render.getvars()
    def POST(self):        
		return render.getvars()

if __name__ == "__main__": 
    app = web.application(urls, globals())
    app.run()
    